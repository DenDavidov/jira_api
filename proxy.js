const http = require('http');
const credentials = require('./credentials.json');

const handler = function(req, res) {
  const options = {
    host: 'jira.bssys.com',
    port: 80,
    method: req.method,
    path: req.originalUrl,
    headers: {
      'Content-Type': 'application/json'
    },
    auth: `${credentials.login}:${credentials.password}`
  };
  const req2 = http.request(options, (res2) => {
    var result = [];
    res2.setEncoding('utf8');
    res2.on('data', (chunk) => {
      result.push(chunk);
    });
    res2.on('end', () => {
      res.json(JSON.parse(result.join('')));
    });

  });
  req2.on('error', (e) => {
    //console.log(`problem with request: ${e.message}`);
  });
  req2.end();
};

const setProxy = (app) => {
  app.route('/rest/*')
    .get(function(req, res, next) {
      handler(req, res);
    })
    .post(function(req, res, next) {
      handler(req, res);
    })
    .delete(function(req, res, next) {
      handler(req, res);
    })
    .put(function(req, res, next) {
      handler(req, res);
    });
};

module.exports = setProxy;

