import 'babel-polyfill'; // generators
import React from 'react';
import { render as renderReact } from 'react-dom';
import App from './containers/app';

const render = (Component) => {
  renderReact(<Component/>, document.getElementById('root'));
};

render(App);

if (module.hot) {
  module.hot.accept('./containers/app', function() {
    render(App);
  });
}