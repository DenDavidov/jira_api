import React from 'react';

class Issue extends React.Component {
  render() {
    const data = {...this.props.data};
    return (
      <div>
        <h1>{ data.key } { data.fields.summary }</h1>
      </div>
    )
  }
}

export default class IssueList extends React.Component {
  _renderIssues(issues) {
    return issues.map(function(issue) {
      return <Issue data = { issue }/>
    })
  }

  render() {
    const issues = this.props.issues;
    return (
      <div> { this._renderIssues(issues)} </div>
    )
  }
}