import React, {Component} from 'react';
import Request from 'superagent/superagent';
import IssueList from '../../components/issueList';
import {ipcRenderer} from 'electron';

const API  = 'http://localhost:3000';
const URL = '/rest/api/2/search?jql=assignee+%3D+DavDA+AND+status+not+in+(%D0%97%D0%B0%D0%BA%D1%80%D1%8B%D1%82,+%D0%9E%D1%82%D0%BA%D0%B0%D0%B7%D0%B0%D0%BD)';

const PING_INTERVAL = 60*1000;

export default class IssuesContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    }
  }

  _startPing() {
    const worker = ()=> {
      this._fetchIssues();
    };

    setInterval(worker.bind(this), PING_INTERVAL)
  }

  _processResponse(err, res) {
    if (err || !res.ok) {
      console.log('Oh no! error');
    } else {
      const oldIssues = JSON.stringify(this.state.data);
      this.setState({data: res.body});
      const newIssues = JSON.stringify(this.state.data);
      if (oldIssues !== newIssues) {
        ipcRenderer.send('got-new-issues');
      }
    }
  }

  _fetchIssues() {
    Request
      .get(API + URL)
      .set('Content-Type', 'application/json; charset=utf-8')
      .end(::this._processResponse);
  }

  componentWillMount() {
    this._fetchIssues();
    this._startPing();
  }

  render() {
    if (this.state.data) {
      return <IssueList { ...this.state.data }/>
    }
    return <div>Идет загрузка</div>
  }
}